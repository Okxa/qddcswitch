# qddcswitch

A Simple frontend for switching display inputs with [ddcutil](https://github.com/rockowitz/ddcutil).

![](screenshots/screenshot_tmp.png)

## About

### Features

- Scans displays and all their inputs, allowing you to select which ones to control.
- Control the inputs of the selected displays from a simple window.
- Refresh the input state of the selected displays manually (to circumvent desync).
- Set custom names for displays.
- Save and load displays to/from file (w/ command line switch even on startup!).

### Usage:

```
Usage: qddcswitch [options]
A simple GUI for switching display inputs using libddcutil

Options:
  -h, --help                        Displays help on commandline options.
  --help-all                        Displays help including Qt specific
                                    options.
  -v, --version                     Displays version information.
  -m, --monitorfile <path to file>  Path to the saved monitors .json file which
                                    to load on startup.
```

- This application should obey your desktops icon theme too. But if you are using any desktop which Qt does not automatically detect, you can force it to load for example gnome icons with: `env XDG_CURRENT_DESKTOP=gnome qddcswitch`. Qt then will try to find a correct icon theme, otherwise it uses the Qt default icons (Shown in the screenshot).

## Downloads

### Dependencies

- Qt5
- libddcutil

### Packages available

Downloads/packages currently provided for the following distros:

- Arch Linux AUR: https://aur.archlinux.org/packages/qddcswitch

I would be thankfull if you want to package this to your distro, or even flatpack etc.

## Building

### Requirements

Same as dependencies overall, but:

- qmake (should come with qt5)
- lupdate/lrelease are used for updating translations on build. This might be in a separate package, depending on your distro.

### How to compile

This application can be compiled with the provided shell scripts `build_debug.sh` or `build_release.sh`, for debug and release builds respectively.

```
build_(release/debug).sh <app data path>`

Arguments:
  <app data path> (Optional) - used to define where the application looks for other application files, such as translations.

Example:
  build_release.sh /usr/share

  Application will look for the folder /user/share/qddcswitch
```

#### **Note 1!**

> If `<app data path>` is **not** supplied, **qddcswitch will instead look for the extra files relative to the executable folder (on runtime)**. Therefore the parameter is mostly usefull when building this for packaging or otherwise.

#### **Note 2!**

> Do **not** append **"/"**
>
> For example:
>
> Correct: `build_release.sh /usr/share`  
> Incorrect: Correct: `build_release.sh /usr/share/`

Using those scripts, the builds are placed in `build_debug` and `build_release` based on which script is run.

## Translations

To add translations for this application:

1. Edit `qddcswitch.pro`, and append the new language file to the `TRANSLATIONS` key, for example:

        From:
        TRANSLATIONS += \
                locale/qddcswitch_en.ts \
                locale/qddcswitch_fi.ts

        To:
        TRANSLATIONS += \
                locale/qddcswitch_en.ts \
                locale/qddcswitch_fi.ts \
                locale/qddcswitch_de.ts

2. Either Run `generate_translations.sh` (uses `lupdate`) to generate new `.ts` file for your language, or simply just copy one existing file, in `./locale`

3. Edit the file either with Qt Linquist, or manually with a text editor.

4. Recompile the application. The `qddcswitch.pro` file has instructions to run `lrelease-pro` to generate and then copy the `.qm` files to the build folder, so they should work automatically.


#### **Note 1!**

While the translation files are placed in build folder for example at: `build_release/bin/locale`, the application will look for the translations depending on the `<app data folder>` flag (See Building -> Note 1).

Therefore be sure to place the folder accordingly.

#### **Note 2!**

The program loads `.qm` files based on your system locale (`env LANGUAGE`). So it should load a translation without recompiling the application too.

Last, you can add your credit(s) to `res/translators.json` if you want to:

```json
...
    {
        "languageCode": "fi",
        "translators": [ "Okxa" ]
    },
    {
        "languageCode": "de",
        "translators": [ "Dr. Translator", "Another Translator" ]
    }
]
```


## Known Issues

#### Issues concerning improper DCC implementation of a display

Issues like a display not reporting the correct capabilites or input values. Might result in incorrect display of input state. Nothing I or ddcutil can fix.
