# This file is part of qddcswitch
#
#   Copyright 2021 Asko Ropponen
#
#   qddcswitch is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   qddcswitch is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with qddcswitch.  If not, see <https://www.gnu.org/licenses/>.
#

QT       += core gui uitools

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qddcswitch
TEMPLATE = app
VERSION = 1.0.0.0

# to access application info from code
DEFINES += APP_NAME=\\\"$$TARGET\\\"
DEFINES += APP_VERSION=\\\"$$VERSION\\\"
DEFINES += "APP_COPYRIGHT=\"\\\"Copyright 2021\\\"\""
DEFINES += "APP_COPYRIGHTHOLDERS=\"\\\"Asko Ropponen\\\"\""

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        src/AddMonitorDialog.cpp \
        src/DDCManager.cpp \
        src/main.cpp \
        src/qddcswitch.cpp

HEADERS += \
        src/AddMonitorDialog.h \
        src/DDCManager.h \
        src/qddcswitch.h

FORMS += \
        forms/AddMonitorDialog.ui \
        res/MonitorInputToggle.ui \
        forms/qddcswitch.ui

TRANSLATIONS += \
        locale/qddcswitch_en.ts \
        locale/qddcswitch_fi.ts

# put build binary in bin folder
DESTDIR = bin
# put all generated files in a folder
OBJECTS_DIR = generated
MOC_DIR = generated
UI_DIR = generated
RCC_DIR = generated

# create custom build steps
# run lrelease to generate .qm files from the .ts files
lrelease.commands = lrelease $$PWD/$$TARGET\.pro
# make a folder, copy translation files to the folder
mkdir.commands = $(MKDIR) $$OUT_PWD/$$DESTDIR/locale
copydata.commands = $(COPY) $$PWD/locale/*.qm $$OUT_PWD/$$DESTDIR/locale
# do the thing to run the steps
mkdir.depends = lrelease
copydata.depends = mkdir
first.depends = copydata
export(first.depends)
export(lrelease.commands)
export(mkdir.commands)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += lrelease mkdir copydata first

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

unix:!macx: LIBS += -lddcutil

RESOURCES += \
    res/mainResources.qrc

DISTFILES += \
    res/contributors.json \
    res/translators.json
