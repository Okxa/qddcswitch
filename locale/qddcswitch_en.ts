<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AddMonitorDialog</name>
    <message>
        <location filename="../forms/AddMonitorDialog.ui" line="+14"/>
        <source>Add a Display</source>
        <extracomment>The title of the add display dialog.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Select a display</source>
        <extracomment>Header above the list in the add display dialog, telling the user to select a display.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Custom Name</source>
        <extracomment>Label indicating that the user can type a custom name for the added display, which is shown in the main window list.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/AddMonitorDialog.cpp" line="+51"/>
        <source>SN</source>
        <extracomment>Abbreviation of the display serial number in the add display list (keep short)</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qddcswitch</name>
    <message>
        <location filename="../forms/qddcswitch.ui" line="+33"/>
        <source>Displays</source>
        <extracomment>The title above the display list in main window.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qddcswitch.cpp" line="+42"/>
        <source>All Files</source>
        <extracomment>Filter name for All Files (*) in the dialog</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Main Menu</source>
        <extracomment>Main Menu label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Add a new display</source>
        <extracomment>Shown on action to add new displays</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save displays to file</source>
        <extracomment>Shown on action to save current displays</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load displays from file</source>
        <extracomment>Shown on action to load displays from file</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Refresh inputs</source>
        <extracomment>Shown on action to refresh current display(s) input states</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Quit</source>
        <extracomment>Shown on action to quit the program</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Help</source>
        <extracomment>Help Menu label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Report Issues</source>
        <extracomment>Shown on action to go to issue tracker (in browser...)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>About</source>
        <comment>About action</comment>
        <extracomment>Shown on action to open about dialog</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+41"/>
        <source>About</source>
        <comment>dialog title</comment>
        <extracomment>About window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Licence</source>
        <extracomment>Shown in about window, before the GPLv3 display.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+43"/>
        <source>Credits</source>
        <extracomment>Credits window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Base language</source>
        <extracomment>Shown for the base untranslated language (english) in credits window</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Developers</source>
        <extracomment>title for developers in the credits window</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Translators</source>
        <extracomment>title for translators in the credits window</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Model</source>
        <extracomment>Text prepending the display model in the main list.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SN</source>
        <extracomment>Abbreviation of the display serial number in the main display list (keep short)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Save Displays</source>
        <extracomment>Save Monitors to file dialog title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Load Displays</source>
        <extracomment>Load Monitors from file dialog title</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
