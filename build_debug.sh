mkdir build_debug
cd build_debug
if [ -n "$1" ]; then
    echo "APP_DATA_FOLDER: $1"
    qmake CONFIG+=debug 'DEFINES+="APP_DATA_FOLDER=\"\\\"'"$1"'\\\"\""' ../qddcswitch.pro
else
    qmake CONFIG+=debug ../qddcswitch.pro
fi
make -j$(nproc)
