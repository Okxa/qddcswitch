/*This file is part of qddcswitch

    Copyright 2021 Asko Ropponen

    qddcswitch is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qddcswitch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qddcswitch.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DDCMANAGER_H
#define DDCMANAGER_H

#include <QObject>
#include <ddcutil_c_api.h>
#include <ddcutil_macros.h>
#include <ddcutil_status_codes.h>
#include <ddcutil_types.h>

class DDCManager : public QObject
{
    Q_OBJECT
public:
    DDCManager();
    static DDCManager *getInstance();
    void scanMonitors();
    DDCA_Display_Info_List *getMonitors();
    DDCA_Display_Info getMonitorInfoByIndex(int index);
    DDCA_Capabilities *getMonitorCapabilitesByIndex(int index);
    QList<QMap<QString, QVariant>> getMonitorInputsByIndex(int index);
    QMap<QString, QVariant> getCurrentInput(int index);
signals:
    /**
     * @brief Emitted when input change has been succesfull.
     * @param activeinput - The value of the new input.
     * @param parent - The MonitorInputToggle to pass back to update the buttons in it.
     */
    void inputChangeFinished(int activeinput, QObject *parent);
public slots:
    void setInputByIndex(int index, int inputvalue, QObject *parent);
private:
    DDCA_Display_Info_List *displayList;
    bool setVcp(DDCA_Display_Handle handle, DDCA_Vcp_Feature_Code feature, uint8_t value_hi_byte, uint8_t value_low_byte);

};

#endif // DDCMANAGER_H
