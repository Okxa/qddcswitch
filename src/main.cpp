/*This file is part of qddcswitch

    Copyright 2021 Asko Ropponen

    qddcswitch is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qddcswitch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qddcswitch.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "qddcswitch.h"
#include <QApplication>
#include <QDebug>
#include <QCommandLineParser>
#include <QTranslator>
#include <QLibraryInfo>

int main(int argc, char *argv[])
{
    // to fix a warning:
    // Qt WebEngine seems to be initialized from a plugin.
    // Please set Qt::AA_ShareOpenGLContexts using QCoreApplication::setAttribute before constructing QGuiApplication.
    QApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QApplication a(argc, argv);
    QApplication::setApplicationName(APP_NAME);
    QApplication::setApplicationVersion(APP_VERSION); // used with --version

    // parse command line arguments
    QCommandLineParser parser;
    parser.setApplicationDescription("A simple GUI for switching display inputs using libddcutil");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption monitorfile(QStringList() << "m" << "monitorfile",
                                   "Path to the saved monitors .json file which to load on startup.",
                                   "path to file");
    parser.addOption(monitorfile);

    parser.process(a);

    // TRANSLATIONS
    // check if we have defined location for translations in build define
    #ifdef APP_DATA_FOLDER
        // if we have, that is the place we look for them in
        QString translationpath = QString(APP_DATA_FOLDER) + "/" + QString(APP_NAME) + "/locale";
    #else
        // otherwise search for them the application folder
        QString translationpath = QCoreApplication::applicationDirPath() + "/locale";
    #endif
    #ifdef QT_DEBUG
        qDebug() << "translation path:" << translationpath;
    #endif
    QTranslator translator; // initialize translator
    // try loading a translation per users locale (eg. LANGUAGE=fi)
    if (translator.load(QLocale(), APP_NAME, "_", translationpath)) {
        a.installTranslator(&translator); // install the translator
    }
    // load qt translations too, for QDialogs etc.
    QTranslator qtTranslator;
    if(qtTranslator.load(QLocale(), "qt", "_", QLibraryInfo::location(QLibraryInfo::TranslationsPath))) {
        a.installTranslator(&qtTranslator);
    }
    qddcswitch w;
    w.setWindowIcon(QIcon(":/icons/qddcswitch-icon.svg"));
    w.show();

    w.loadMonitorsFromFile(parser.value(monitorfile));
    return a.exec();
}
