/*This file is part of qddcswitch

    Copyright 2021 Asko Ropponen

    qddcswitch is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qddcswitch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qddcswitch.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "AddMonitorDialog.h"
#include "ui_AddMonitorDialog.h"

/**
 * @brief Constructs the AddMonitorDialog
 * @param parent -  Qt default param.
 */
AddMonitorDialog::AddMonitorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddMonitorDialog)
{
    ui->setupUi(this);
    ddcManager = DDCManager::getInstance();
    monitorListWidget = ui->monitorListWidget;
    populateMonitorList();
    connect(ui->monitorListWidget, &QListWidget::itemDoubleClicked, this, &AddMonitorDialog::confirmMonitor);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &AddMonitorDialog::confirmMonitor);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &AddMonitorDialog::close);
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setIcon(QIcon::fromTheme("edit-delete", this->style()->standardIcon(QStyle::SP_DialogCancelButton)));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setIcon(QIcon::fromTheme("list-add", this->style()->standardIcon(QStyle::SP_DialogOkButton)));
}
/**
 * @brief Destructor
 */
AddMonitorDialog::~AddMonitorDialog()
{
    delete ui;
}
/**
 * @brief Creates the monitor list items in the add monitor dialog.
 */
void AddMonitorDialog::populateMonitorList() {
    monitorList = ddcManager->getMonitors();
    for (int i = 0; i < monitorList->ct; i++) {
        //: Abbreviation of the display serial number in the add display list (keep short)
        new QListWidgetItem(QString(monitorList->info[i].model_name) + " " + tr("SN") + ": " + QString(monitorList->info[i].sn), monitorListWidget);
    }
}
/**
 * @brief Handles the selection of a new monitor to add.
 */
void AddMonitorDialog::confirmMonitor() {
    emit AddMonitorDialog::selectMonitor(monitorListWidget->currentRow(), ui->lineEditCustomName->text());
    ui->lineEditCustomName->setText("");
    this->close();
}

