/*This file is part of qddcswitch

    Copyright 2021 Asko Ropponen

    qddcswitch is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qddcswitch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qddcswitch.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "DDCManager.h"
#include <QGlobalStatic>
#include <QDebug>

Q_GLOBAL_STATIC(DDCManager, ddcManager)
/**
 * @brief Constructs the DDCManager
 */
DDCManager::DDCManager()
{

}
/**
 * @brief Get an instance of this Class
 * @return The Instance
 */
DDCManager* DDCManager::getInstance() {
    return ddcManager;
}
/**
 * @brief Gets the displays from libddcutil to displaylist
 */
void DDCManager::scanMonitors() {
    ddca_get_display_info_list2(true, &displayList);
}
/**
 * @brief Get a pointer to displaylist from DDCManager
 * @return Pointer to displaylist
 */
DDCA_Display_Info_List *DDCManager::getMonitors() {
    if (!displayList) {
        scanMonitors();
    }
    return displayList;
}
/**
 * @brief Get all info of a monitor by the index of the monitor.
 * @param index - The index of the monitor in displaylist
 * @return A libddcutil object containing information about the display
 */
DDCA_Display_Info DDCManager::getMonitorInfoByIndex(int index) {
    DDCA_Display_Info monitorInfo = displayList->info[index];
    return monitorInfo;
}
/**
 * @brief Get capabilities by the index of the monitor.
 * @param index - The index of the monitor in displaylist
 * @return A libddcutil object containing capabilities of the display.
 */
DDCA_Capabilities *DDCManager::getMonitorCapabilitesByIndex(int index) {
    // get handle
    DDCA_Display_Handle handle;
    ddca_open_display2(getMonitorInfoByIndex(index).dref, false, &handle);
    // get capabilities
    char *capabilities_string;
    ddca_get_capabilities_string(handle, &capabilities_string);
    DDCA_Capabilities *capabilities;
    ddca_parse_capabilities_string(capabilities_string, &capabilities);
    // close handle
    ddca_close_display(handle);
    return capabilities;
}
/**
 * @brief Get the available inputs of a display.
 * @param index - The index of the monitor in displaylist
 * @return A QList of inputs (input = QMap containing the inputs code, name and if currently enabled).
 */
QList<QMap<QString, QVariant>> DDCManager::getMonitorInputsByIndex(int index) {
    QList<QMap<QString, QVariant>> inputs;

    // Get capabilities
    DDCA_Capabilities *capabilities = getMonitorCapabilitesByIndex(index);

    // open handle afterwards (getMonitorCapabilitesByIndex opens and closes handle)
    DDCA_Display_Handle handle;
    ddca_open_display2(getMonitorInfoByIndex(index).dref, false, &handle);

    // loop through capabilities to find input feature & possible values
    // TODO: SLOW; possible to do otherwise?
    for (int i = 0; i < capabilities->vcp_code_ct; i++) {
        DDCA_Cap_Vcp *currentVpc = &capabilities->vcp_codes[i];
        // input feature
        if (currentVpc->feature_code == 0x60) {
            // get metadata about the feature
            DDCA_Feature_Metadata *metadata;
            ddca_get_feature_metadata_by_dh(currentVpc->feature_code, handle, false, &metadata);

            DDCA_Feature_Value_Entry *valuetable;
            valuetable = metadata->sl_values;

            #ifdef QT_DEBUG
            qDebug() << "Feature:" << metadata->feature_code << metadata->feature_name;
            #endif

            // read the current value
            DDCA_Non_Table_Vcp_Value currentvalue;
            ddca_get_non_table_vcp_value(handle, currentVpc->feature_code, &currentvalue);
            // read the value as formatted (eg. VGA-1 for input)
            char *currentvalueformatted;
            ddca_format_non_table_vcp_value_by_dref(currentVpc->feature_code,  getMonitorInfoByIndex(index).dref, &currentvalue, &currentvalueformatted);

            #ifdef QT_DEBUG
            qDebug() << "Current value:" << currentvalue.sl << currentvalueformatted;
            #endif

            #ifdef QT_DEBUG
            qDebug() << "Possible values:";
            #endif
            // parse possible values
            // loop through the possible values
            for (int j = 0; j < currentVpc->value_ct; j++) {
                uint8_t current_possible_value = currentVpc->values[j];
                // loop through metadata to get info on the values:
                bool done = false;
                int  k = 0;
                do {
                    if (valuetable[k].value_code == 0x00) {
                        // if value code is 0x00, the table has ended.
                        done = true;
                    }
                    else {
                        // the value code found in metadata is the current possible value
                        if (valuetable[k].value_code == current_possible_value) {

                            #ifdef QT_DEBUG
                            //qDebug() << "mh" << currentvalue.mh << "ml" << currentvalue.ml << "sh" << currentvalue.sh << "sl" << currentvalue.sl;
                            qDebug() << valuetable[k].value_code << valuetable[k].value_name;
                            #endif
                            QMap<QString, QVariant> input;
                            input.insert("code", valuetable[k].value_code);
                            input.insert("name", valuetable[k].value_name);
                            if (current_possible_value == currentvalue.sl) {
                                input.insert("selected", true);
                            }
                            else {
                                input.insert("selected", false);
                            }
                            inputs.append(input);
                        }
                        k++;
                    }
                } while (done == false);
            }
        }
    }
    ddca_close_display(handle);
    return inputs;
}
/**
 * @brief Get the current input value of a display.
 * @param index - The index of the monitor in displaylist
 * @return The value of the input feature.
 */
QMap<QString, QVariant> DDCManager::getCurrentInput(int index) {
    DDCA_Display_Handle handle;
    ddca_open_display2(getMonitorInfoByIndex(index).dref, false, &handle);
    // read the current value
    DDCA_Non_Table_Vcp_Value currentvalue;
    ddca_get_non_table_vcp_value(handle, 0x60, &currentvalue);
    // read the value as formatted (eg. VGA-1 for input)
    char *currentvalueformatted;
    ddca_format_non_table_vcp_value_by_dref(0x60,  getMonitorInfoByIndex(index).dref, &currentvalue, &currentvalueformatted);

    QMap<QString, QVariant> input;
    input.insert("code", currentvalue.sl);
    input.insert("name", currentvalueformatted);
    ddca_close_display(handle);
    return input;
}
/**
 * @brief Sets the input of a monitor.
 * @param index - The index of the monitor in displaylist
 * @param inputvalue - The value of the new input.
 * @param parent - Passed through to return back to UI to update the buttons in the correct MonitorInputToggle.
 */
void DDCManager::setInputByIndex(int index, int inputvalue, QObject *parent) {
    // again need a handle
    DDCA_Display_Handle handle;
    ddca_open_display2(getMonitorInfoByIndex(index).dref, false, &handle);
    bool success = setVcp(handle, 0x60, inputvalue, inputvalue);
    ddca_close_display(handle);
    if (success == true) {
        emit inputChangeFinished(inputvalue, parent);
    }
}
/**
 * @brief Sets any vcp feature
 * @param handle - A libddcutil display handle
 * @param feature - the vcp feature code to change.
 * @param value_hi_byte - the value to set, hi byte
 * @param value_low_byte - the value to set, low byte
 * @return True if input change was succesfull, false otherwise.
 */
bool DDCManager::setVcp(DDCA_Display_Handle handle, DDCA_Vcp_Feature_Code feature, uint8_t value_hi_byte, uint8_t value_low_byte) {
    // try setting value and read status
    // and do some bitwise operations for the values because fuck if I know
    DDCA_Status status = ddca_set_non_table_vcp_value(handle, feature, value_hi_byte >> 8, value_low_byte & 0xff);
    if (status != 0) {
        qDebug() << ddca_rc_name(status) << ddca_rc_desc(status);
        return false;
    }
    else {
        qDebug() << "Set value correctly";
        return true;
    }
}
