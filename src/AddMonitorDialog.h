/*This file is part of qddcswitch

    Copyright 2021 Asko Ropponen

    qddcswitch is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qddcswitch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qddcswitch.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef ADDMONITORDIALOG_H
#define ADDMONITORDIALOG_H

#include <QDialog>
#include <QListWidget>
#include <QPushButton>
#include <QDebug>
#include "DDCManager.h"

namespace Ui {
class AddMonitorDialog;
}

class AddMonitorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddMonitorDialog(QWidget *parent = nullptr);
    ~AddMonitorDialog();
signals:
    /**
     * @brief Emitted when monitor has been confirmed.
     * @param index - the index of the monitor in displaylist.
     * @param customName - The custom name of the display set in the dialog.
     */
    void selectMonitor(int index, QString customName);
private:
    DDCManager *ddcManager;
    Ui::AddMonitorDialog *ui;
    QListWidget *monitorListWidget;
    DDCA_Display_Info_List *monitorList;
    void populateMonitorList();
private slots:
    void confirmMonitor();
};

#endif // ADDMONITORDIALOG_H
