/*This file is part of qddcswitch

    Copyright 2021 Asko Ropponen

    qddcswitch is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qddcswitch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qddcswitch.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "qddcswitch.h"
#include "ui_qddcswitch.h"
#include <QDebug>
#include <QtUiTools/QUiLoader>
#include <QFile>
#include <QPushButton>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFileDialog>
#include <QMessageBox>
#include <QDesktopServices>

/**
 * @brief Construct the main window.
 * @param parent - Qt Default param.
 */
qddcswitch::qddcswitch(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::qddcswitch)
{
    ui->setupUi(this);

    // for translations
    //: Filter name for All Files (*) in the dialog
    allFilesFilterName = tr("All Files");

    // get instance of DDCManager
    ddcManager = DDCManager::getInstance();

    // create menu
    //: Main Menu label
    QMenu *mainMenu = new QMenu(tr("Main Menu"), ui->menuBar);
    // create actions & add them to menu & toolbar
    // add monitor dialog open
    //: Shown on action to add new displays
    addNewMonitorAction = new QAction(tr("Add a new display") + "...");
    #ifdef QT_DEBUG
        qDebug() << "Icon theme selected:" << QIcon::themeName();
    #endif
    addNewMonitorAction->setIcon(QIcon::fromTheme("list-add", this->style()->standardIcon(QStyle::SP_TitleBarMaxButton)));
    connect(addNewMonitorAction, &QAction::triggered, this, &qddcswitch::openAddMonitorDialog);
    mainMenu->addAction(addNewMonitorAction);
    ui->mainToolBar->addAction(addNewMonitorAction);
    // save action
    //: Shown on action to save current displays
    saveAction = new QAction(tr("Save displays to file") + "...");
    saveAction->setIcon(QIcon::fromTheme("document-save", this->style()->standardIcon(QStyle::SP_DialogSaveButton)));
    connect(saveAction, &QAction::triggered, this, &qddcswitch::saveMonitors);
    ui->mainToolBar->addAction(saveAction);
    mainMenu->addAction(saveAction);
    // load action
    //: Shown on action to load displays from file
    loadAction = new QAction(tr("Load displays from file") + "...");
    loadAction->setIcon(QIcon::fromTheme("document-open", this->style()->standardIcon(QStyle::SP_DialogOpenButton)));
    connect(loadAction, &QAction::triggered, this, &qddcswitch::loadMonitors);
    ui->mainToolBar->addAction(loadAction);
    mainMenu->addAction(loadAction);
    // refresh action
    //: Shown on action to refresh current display(s) input states
    refreshAction = new QAction(tr("Refresh inputs"));
    refreshAction->setIcon(QIcon::fromTheme("view-refresh", this->style()->standardIcon(QStyle::SP_BrowserReload)));
    connect(refreshAction, &QAction::triggered, this, &qddcswitch::refreshAllInputs);
    ui->mainToolBar->addAction(refreshAction);
    mainMenu->addAction(refreshAction);
    // quit
    //: Shown on action to quit the program
    quitAction = new QAction(tr("Quit"));
    quitAction->setIcon(QIcon::fromTheme("application-exit", this->style()->standardIcon(QStyle::SP_DialogCancelButton)));
    connect(quitAction, &QAction::triggered, QApplication::instance(), &QApplication::quit);
    mainMenu->addAction(quitAction);

    // add menu to menubar
    ui->menuBar->addMenu(mainMenu);

    // create help menu
    //: Help Menu label
    QMenu *helpMenu = new QMenu(tr("Help"), ui->menuBar);
    // go to git repo issues action
    //: Shown on action to go to issue tracker (in browser...)
    reportIssueAction = new QAction(tr("Report Issues"));
    reportIssueAction->setIcon(QIcon::fromTheme("insert-link", this->style()->standardIcon(QStyle::SP_CommandLink)));
    connect(reportIssueAction, &QAction::triggered, this, &qddcswitch::openIssueTracker);
    helpMenu->addAction(reportIssueAction);
    // show about dialog
    //: Shown on action to open about dialog
    aboutAction = new QAction(tr("About", "About action") + "...");
    aboutAction->setIcon(QIcon::fromTheme("help-about", this->style()->standardIcon(QStyle::SP_DialogHelpButton)));
    connect(aboutAction, &QAction::triggered, this, &qddcswitch::openAboutDialog);
    helpMenu->addAction(aboutAction);
    ui->menuBar->addMenu(helpMenu);
    // do connections
    connect(&addMonitorDialog, &AddMonitorDialog::selectMonitor, this, &qddcswitch::addToggle);
    connect(this, &qddcswitch::changeInput, ddcManager, &DDCManager::setInputByIndex);
    connect(ddcManager, &DDCManager::inputChangeFinished, this, &qddcswitch::updateButtons);
}
/**
 * @brief Destructor
 */
qddcswitch::~qddcswitch()
{
    delete ui;
}
/**
 * @brief Opens the dialog to add monitors to the main list.
 */
void qddcswitch::openAddMonitorDialog() {
    addMonitorDialog.setWindowIcon(QIcon(":/icons/qddcswitch-icon.svg"));
    addMonitorDialog.show();
}

void qddcswitch::openAboutDialog()
{
    QString licencestring ="qddcswitch is free software: you can redistribute it and/or modify \n\
it under the terms of the GNU General Public License as published by \n\
the Free Software Foundation, either version 3 of the License, or \n\
(at your option) any later version. \n\
\n\
qddcswitch is distributed in the hope that it will be useful, \n\
but WITHOUT ANY WARRANTY; without even the implied warranty of \n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the \n\
GNU General Public License for more details. \n\
\n\
You should have received a copy of the GNU General Public License \n\
along with qddcswitch.  If not, see <a href='http://www.gnu.org/licenses/'>https://www.gnu.org/licenses/</a>.";

    QMessageBox about;
    about.setWindowIcon(QIcon(":/icons/qddcswitch-icon.svg"));
    //: About window title
    about.setWindowTitle(tr("About", "dialog title") + " " + QString(APP_NAME));
    about.setMinimumWidth(120);
    about.setTextFormat(Qt::RichText);
    //: Shown in about window, before the GPLv3 display.
    QString licencetitle = tr("Licence");
    about.setText("<h3>" + QApplication::applicationName() + " " + QApplication::applicationVersion() + "</h3>" +
                  "</p>" + QString(APP_COPYRIGHT) + " " + QString(APP_COPYRIGHTHOLDERS) + "</p>" +
                  "<h3>" + licencetitle + ": GPLv3</h3>" +
                  "<pre>" + licencestring + "</pre>");
    about.setStandardButtons(QMessageBox::Close);
    QAbstractButton *creditsBtn = about.addButton(tr("Credits") + "...", QMessageBox::NoRole);
    creditsBtn->disconnect();
    connect(creditsBtn, &QAbstractButton::clicked, this, &qddcswitch::openCreditsDialog);
    about.exec();
}

void qddcswitch::openCreditsDialog() {
    QString contribstring = "<ul>";
    QFile contribFile(":/contrib/contributors.json");
    if (contribFile.open(QIODevice::ReadOnly)) {
        QByteArray jsonStr = contribFile.readAll();
        QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonStr);
        QJsonArray contributors = jsonDoc.array();
        contribFile.close();
        for (QJsonValueRef contrib : contributors) {
            contribstring.append("<li'>" + contrib.toString() + "</li>");
        }
        contribstring.append("</ul>");
    }
    QString translatorstring;
    QFile translatorFile(":/contrib/translators.json");
    if (translatorFile.open(QIODevice::ReadOnly)) {
        QByteArray jsonStr = translatorFile.readAll();
        QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonStr);
        QJsonArray contributors = jsonDoc.array();
        translatorFile.close();
        for (QJsonValueRef language : contributors) {
            QString langCode = language.toObject()["languageCode"].toString();
            QLocale loc(langCode);
            QString languageName = QLocale::languageToString(loc.language());
            //: Shown for the base untranslated language (english) in credits window
            QString baselang = tr("Base language");
            translatorstring.append("<b>" + ((langCode == "en") ? languageName + " (" + baselang + ")" : loc.nativeLanguageName() + " (" + languageName + ")") + "</b><ul>");
            QJsonArray translators = language.toObject()["translators"].toArray();
            for (QJsonValueRef translator : translators) {
                translatorstring.append("<li>" + translator.toString() + "</li>");
            }
            translatorstring.append("</ul>");
        }
    }
    QMessageBox cred;
    //: Credits window title
    cred.setWindowTitle(tr("Credits"));
    cred.setMinimumWidth(120);
    cred.setTextFormat(Qt::RichText);
    //: title for developers in the credits window
    QString contributorstitle = tr("Developers");
    //: title for translators in the credits window
    QString translatorstitle = tr("Translators");
    cred.setText("<h3>" + contributorstitle + ":</h3>" + contribstring +
                  "<h3>" + translatorstitle + ":</h3>" + translatorstring);
    cred.setStandardButtons(QMessageBox::Close);
    cred.exec();
}

void qddcswitch::openIssueTracker() {
    QDesktopServices::openUrl(QUrl("https://codeberg.org/Okxa/qddcswitch/issues"));
}
/**
 * @brief Handles creating and adding input toggles to the mainwindow
 * @param index - the index of the monitor. Needs to match the index in the list from DDCManager displaylist.
 * @param customName - Custom monitor name to be shown in the list.
 */
void qddcswitch::addToggle(int index, QString customName) {
    // get display info
    DDCA_Display_Info display = ddcManager->getMonitorInfoByIndex(index);

    // load the sample widget.
    QUiLoader loader;
    QFile file(":/forms/MonitorInputToggle.ui");
    file.open(QFile::ReadOnly);
    QWidget *toggleWidget = loader.load(&file);
    file.close();
    if (!customName.isEmpty()) {
        toggleWidget->findChild<QLabel*>("labelMonitorCustomName")->setText(customName);
        toggleWidget->setProperty("customName", customName);
    }
    else {
        toggleWidget->findChild<QLabel*>("labelMonitorCustomName")->setText(QString(display.model_name));
        toggleWidget->setProperty("customName", display.model_name);
    }
    //: Text prepending the display model in the main list.
    toggleWidget->findChild<QLabel*>("labelMonitorName")->setText(tr("Model") + ": " + QString(display.model_name));
    //: Abbreviation of the display serial number in the main display list (keep short)
    toggleWidget->findChild<QLabel*>("labelMonitorSN")->setText(tr("SN") + ": "+ QString(display.sn));
    toggleWidget->setProperty("displayindex", index); // store the  monitor index no.
    toggleWidget->setProperty("name", display.model_name); // store monitor name to widget
    toggleWidget->setProperty("sn", display.sn); // store serial number to widget
    // find the button container.
    QHBoxLayout *btnContainer = toggleWidget->findChild<QHBoxLayout*>("controlBtnLayout");
    // save the monitor number to the widget

    // get inputs
    QList<QMap<QString, QVariant>> inputs = ddcManager->getMonitorInputsByIndex(index);
    // loop through the inputs and create buttons
    for (QMap<QString, QVariant> item : inputs) {
        QPushButton *selectThisInputBtn = new QPushButton(item["name"].toString());
        selectThisInputBtn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        selectThisInputBtn->setFixedWidth(100);
        selectThisInputBtn->setProperty("valueId", item["code"].toInt());
        if (item["selected"].toBool() == true) {
            selectThisInputBtn->setEnabled(false);
        }
        connect(selectThisInputBtn, &QPushButton::clicked, this, &qddcswitch::handleInputSelect);
        btnContainer->addWidget(selectThisInputBtn);
    }

    ui->monitorToggleList->insertWidget(0, toggleWidget);
}
/**
 * @brief Slot to handle the click of an input change button.
 */
void qddcswitch::handleInputSelect() {
    // get the button to read values from it.
    QPushButton *senderBtn = qobject_cast<QPushButton*>(sender());
    emit changeInput(senderBtn->parent()->property("displayindex").toInt(), senderBtn->property("valueId").toInt(), senderBtn->parent());
}
/**
 * @brief Refreshes every currently used monitors current input status. (Called mainly by refreshAction)
 */
void qddcswitch::refreshAllInputs() {
    // loop through all the MonitorInputToggles
    for (QObject *toggle : ui->centralWidget->findChildren<QWidget*>("monitorInputToggle", Qt::FindDirectChildrenOnly)) {
        // get the current input of the monitor
        QMap<QString, QVariant> input = ddcManager->getCurrentInput(toggle->property("displayindex").toInt());
        // update the buttons, using the value of the current input
        updateButtons(input["code"].toInt(), toggle);
    }
}
/**
 * @brief Updates the input buttons in a single MonitorInputToggle widget.
 * @param activeinput - the value of the currently active input
 * @param parent - the MonitorInputToggle widget to be updated
 */
void qddcswitch::updateButtons(int activeinput, QObject *parent) {
    for (QPushButton *child : parent->findChildren<QPushButton*>()) {
        // loop buttons, if the id matches, it's active -> disable button.
        if (child->property("valueId").toInt() == activeinput) {
            child->setEnabled(false);
        }
        else {
            child->setEnabled(true); // vice versa
        }
    }

}
/**
 * @brief Parses the currently used monitors to JSON, which includes each monitors, customName, name and sn.
 * @return QByteArray JSON representation of the current monitors.
 */
QByteArray qddcswitch::getCurrentMonitorsJSONByteArray() {
    QJsonArray monitors;
    for (QObject *child : ui->centralWidget->findChildren<QWidget*>("monitorInputToggle", Qt::FindDirectChildrenOnly)) {
        qDebug() << "Monitor SN:" << child->property("sn").toInt() << child->property("name");
        QJsonObject monitor;
        monitor.insert("customName", QJsonValue::fromVariant(child->property("customName").toString()));
        monitor.insert("name", QJsonValue::fromVariant(child->property("name").toString()));
        monitor.insert("sn", QJsonValue::fromVariant(child->property("sn").toString()));
        monitors.append(monitor);
    }
    QJsonDocument jsonDoc(monitors);
    QByteArray jsonStr(jsonDoc.toJson(QJsonDocument::Indented));
    return jsonStr;
};
/**
 * @brief Handles saving current monitors. Opens a QFileDialog to select save location. (Mainly used from saveAction)
 */
void qddcswitch::saveMonitors() {
    QByteArray monitorJson = getCurrentMonitorsJSONByteArray();
    //: Save Monitors to file dialog title
    QFileDialog dialog(this, tr("Save Displays"), QDir::homePath(), allFilesFilterName + "(*)");
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setDefaultSuffix("json");
    dialog.setFilter(QDir::AllEntries | QDir::Hidden);
    dialog.exec();
    QString filename = dialog.selectedFiles().first();
    if (!filename.isEmpty()) {
        QFile file(filename);
        if (file.open(QIODevice::WriteOnly)) {
            file.write(monitorJson);
        }
        else {
            qDebug() << "save: Error on fileopen" << file.errorString();
        }
        file.close();
    }
    else {
        qDebug() << "save: Error no filename";
    }
}
/**
 * @brief Handles the input from loadAction
 */
void qddcswitch::loadMonitors() {
    //: Load Monitors from file dialog title
    QFileDialog dialog(this, tr("Load Displays"), QDir::homePath(), allFilesFilterName + "(*)");
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setFilter(QDir::AllEntries | QDir::Hidden);
    dialog.exec();
    QString filename = dialog.selectedFiles().first();
    if (!filename.isEmpty()) {
        loadMonitorsFromFile(filename);
    }
    else {
        qDebug() << "load: Error no filename";
    }
}
/**
 * @brief Loads the saved monitors .json file and if the saved monitor(s) exists, adds the toggles to the main list.
 * @param filepath - the path to the file.
 */
void qddcswitch::loadMonitorsFromFile(QString filepath) {
    if (!filepath.isEmpty()) {
        QFile file(filepath);
        if (file.open(QIODevice::ReadOnly)) {
            QByteArray jsonStr = file.readAll();
            QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonStr);
            QJsonArray monitors = jsonDoc.array();
            DDCA_Display_Info_List *monitorList = ddcManager->getMonitors();
            for (int i = 0; i < monitorList->ct; i++) {
                for (QJsonValueRef monitor : monitors) {
                    QJsonObject monitorObj = monitor.toObject();
                    if (monitorList->info[i].sn == monitorObj["sn"].toString()) {
                        addToggle(i, monitorObj["customName"].toString());
                    }
                }
            }
            file.close();
        }
    }
}
