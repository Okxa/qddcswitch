/*This file is part of qddcswitch

    Copyright 2021 Asko Ropponen

    qddcswitch is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qddcswitch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qddcswitch.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef QDDCSWITCH_H
#define QDDCSWITCH_H

#include <QMainWindow>
#include "AddMonitorDialog.h"
#include "DDCManager.h"

namespace Ui {
class qddcswitch;
}

class qddcswitch : public QMainWindow
{
    Q_OBJECT

public:
    explicit qddcswitch(QWidget *parent = nullptr);
    ~qddcswitch();
    void loadMonitorsFromFile(QString filepath);
signals:
    /**
     * @brief Emitted when a input change button is clicked.
     * @param displayindex - The index of the display in displaylist.
     * @param inputvalue - The value of the input to be set.
     * @param parent - The MonitorInputToggle which the button is in.
     */
    void changeInput(int displayindex, int inputvalue, QObject *parent);
public slots:
    void addToggle(int index, QString customName);
    void handleInputSelect();
    void refreshAllInputs();
    void updateButtons(int activeinput, QObject *parent);
private:
    // funcs
    QByteArray getCurrentMonitorsJSONByteArray();
    void saveMonitors();
    void loadMonitors();
    void openAddMonitorDialog();
    void openAboutDialog();
    void openCreditsDialog();
    void openIssueTracker();
    // vars
    Ui::qddcswitch *ui;
    QString allFilesFilterName;
    // actions
    QAction *quitAction;
    QAction *refreshAction;
    QAction *saveAction;
    QAction *loadAction;
    QAction *addNewMonitorAction;
    QAction *reportIssueAction;
    QAction *aboutAction;
    // other classes
    AddMonitorDialog addMonitorDialog;
    DDCManager *ddcManager;
};

#endif // QDDCSWITCH_H
