mkdir build_release
cd build_release
if [ -n "$1" ]; then
    echo "APP_DATA_FOLDER: $1"
    qmake 'DEFINES+="APP_DATA_FOLDER=\"\\\"'"$1"'\\\"\""' ../qddcswitch.pro
else
    qmake ../qddcswitch.pro
fi
make -j$(nproc)
